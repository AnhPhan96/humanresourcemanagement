class Department < ApplicationRecord
	has_many :staffs, dependent: :destroy
end
