class Staff < ApplicationRecord
	# searchkick
	# Staff.reindex
	belongs_to :department
	default_scope -> { order(created_at: :desc) }
	before_save { self.email = email.downcase }

	# VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	# validates :email, presence: true, length: { maximum: 255 },
	#                     format: { with: VALID_EMAIL_REGEX }            
end
