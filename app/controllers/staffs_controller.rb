class StaffsController < ApplicationController
	def show
  
end

def index
    # if params[:query].present?
    #       @staffs = Staff.search params[:query], page: params[:page], per_page: 30
    # else
      @staffs = Staff.all.paginate(page: params[:page])
    # end
  end

def create
    
  end

def edit
    @staff = Staff.find(params[:id])
  end

def update
    
  end

  def new
    @staff = Staff.new
  end

  def find id
    staff = Staff.find_by(id: id)
  end

  def destroy
    @staff = Staff.find(params[:id])
    @department = Department.find_by(id: @staff.department_id)
    @staff.destroy
    flash[:success] = "Staff deleted"
    redirect_to @department
  end

private
    # Never trust parameters from the scary internet, only allow the white list through.
    def staff_params
      params.require(:staff).permit(:id, :name,:phone, :address, :email, :out, :position, :status, :gender, :department_id)
    end

end