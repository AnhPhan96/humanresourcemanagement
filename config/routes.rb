Rails.application.routes.draw do
  get 'sessions/new'

  resources :departments
  resources :staffs
  resources :admins
  root 'departments#index'
  

  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  get    '/signup',  to: 'admins#new'
end
