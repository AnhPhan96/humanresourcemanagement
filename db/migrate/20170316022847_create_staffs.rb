class CreateStaffs < ActiveRecord::Migration[5.0]
  def change
    create_table :staffs do |t|
      t.string :name
      t.string :phone
      t.string :address
      t.string :position
      t.string :idcard
      t.integer :salary
      t.string :gender
      t.string :email
      t.string :status
      t.boolean :out
      t.references :department, foreign_key: true

      t.timestamps
    end
    add_index :staffs, [:department_id, :created_at]
  end
end
