# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
1.times do |n|
	department_name = "IT"
	Department.create!(name: department_name
		)
end
1.times do |n|
	department_name = "Marketing"
	Department.create!(name: department_name
		)
end
1.times do |n|
	department_name = "Finance"
	Department.create!(name: department_name
		)
end
1.times do |n|
	department_name =  "Technical"
	Department.create!(name: department_name
		)
end
1.times do |n|
	department_name = "Human Resource"
	Department.create!(name: department_name
		)
end
1.times do |n|
	department_name = "Accountant"
	Department.create!(name: department_name
		)
end

10.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  phone = "000000000#{n+1}"
  address = "Nguyen Luong Bang"
  position = "Staff"
  idcard = "123456#{n+1}"
  salary = 5000000 + n+100
  gender = "Nam"
  status = "Official Staff"
  out = true,
  department_id = 1
  Staff.create!(name:  name,
               email: email,
               phone: phone,
               address: address,
               position: position,
               idcard: idcard,
               salary: salary,
               gender: gender,
               status: status,
               out: out,
               department_id: department_id
               )
end
7.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  phone = "000000000#{n+1}"
  address = "Nguyen Luong Bang"
  position = "Staff"
  idcard = "123456#{n+1}"
  salary = 5000000 + n+100
  gender = "Nam"
  status = "Official Staff"
  out = false,
  department_id = 1
  Staff.create!(name:  name,
               email: email,
               phone: phone,
               address: address,
               position: position,
               idcard: idcard,
               salary: salary,
               gender: gender,
               status: status,
               out: out,
               department_id: department_id
               )
end
82.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  phone = "000000000#{n+1}"
  address = "Nguyen Luong Bang"
  position = "Staff"
  idcard = "123456#{n+1}"
  salary = 5000000 + n+100
  gender = "Nam"
  status = "Official Staff"
  out = true,
  department_id = 1
  Staff.create!(name:  name,
               email: email,
               phone: phone,
               address: address,
               position: position,
               idcard: idcard,
               salary: salary,
               gender: gender,
               status: status,
               out: out,
               department_id: department_id
               )
end

69.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  phone = "000000000#{n+1}"
  address = "Nguyen Luong Bang"
  position = "Staff"
  idcard = "123456#{n+1}"
  salary = 5000000 + n+100
  gender = "Nu"
  status = "Official Staff"
  out = true,
  department_id = 2
  Staff.create!(name:  name,
               email: email,
               phone: phone,
               address: address,
               position: position,
               idcard: idcard,
               salary: salary,
               gender: gender,
               status: status,
               out: out,
               department_id: department_id
               )
end

79.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  phone = "000000000#{n+1}"
  address = "Nguyen Luong Bang"
  position = "Staff"
  idcard = "123456#{n+1}"
  salary = 5000000 + n+100
  gender = "Nam"
  status = "Official Staff"
  out = true,
  department_id = 3
  Staff.create!(name:  name,
               email: email,
               phone: phone,
               address: address,
               position: position,
               idcard: idcard,
               salary: salary,
               gender: gender,
               status: status,
               out: out,
               department_id: department_id
               )
end

50.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  phone = "000000000#{n+1}"
  address = "Nguyen Luong Bang"
  position = "Staff"
  idcard = "123456#{n+1}"
  salary = 5000000 + n+100
  gender = "Nu"
  status = "Official Staff"
  out = true,
  department_id = 4
  Staff.create!(name:  name,
               email: email,
               phone: phone,
               address: address,
               position: position,
               idcard: idcard,
               salary: salary,
               gender: gender,
               status: status,
               out: out,
               department_id: department_id
               )
end

60.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  phone = "000000000#{n+1}"
  address = "Nguyen Luong Bang"
  position = "Staff"
  idcard = "123456#{n+1}"
  salary = 5000000 + n+100
  gender = "Nam"
  status = "Official Staff"
  out = true,
  department_id = 5
  Staff.create!(name:  name,
               email: email,
               phone: phone,
               address: address,
               position: position,
               idcard: idcard,
               salary: salary,
               gender: gender,
               status: status,
               out: out,
               department_id: department_id
               )
end

59.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  phone = "000000000#{n+1}"
  address = "Nguyen Luong Bang"
  position = "Staff"
  idcard = "123456#{n+1}"
  salary = 5000000 + n+100
  gender = "Nu"
  status = "Official Staff"
  out = true,
  department_id = 6
  Staff.create!(name:  name,
               email: email,
               phone: phone,
               address: address,
               position: position,
               idcard: idcard,
               salary: salary,
               gender: gender,
               status: status,
               out: out,
               department_id: department_id
               )
end